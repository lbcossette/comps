from copy import deepcopy
import re

letters = {"A" :  0, "B" :  1, "C" :  2, "D" :  3, "E" :  4,
           "F" :  5, "G" :  6, "H" :  7, "I" :  8, "J" :  9,
           "K" : 10, "L" : 11, "M" : 12, "N" : 13, "O" : 14,
           "P" : 15, "Q" : 16, "R" : 17, "S" : 18, "T" : 19,
           "U" : 20, "V" : 21, "W" : 22, "X" : 23, "Y" : 24,
           "Z" : 25}

# Not actually used
def countingSortByLength(strings):
    maxlength = 0

    for string in strings:
        if len(string) > maxlength:
            maxlength = len(string)

    lengths = list((0,)*(maxlength+1))

    for string in strings:
        lengths[len(string)] += 1

    lengths[0] -= 1

    for i in range(1,len(lengths)):
        lengths[i] += lengths[i-1]

    sortedStrings = list(("",)*len(strings))

    for string in strings:
        length = len(string)

        sortedStrings[lengths[length]] = string

        lengths[length] -= 1

    return sortedStrings



def radixSortStrings(strings, sortingDict):
    """
    Sorts strings according to the order specified
    in the sortingDict.
    """

    maxlength = 0

    sortedStrings = deepcopy(strings)

    # One extra array for "string too short"
    sorter = []
    for i in range(len(sortingDict) + 1):
        sorter.append([])

    # Find end of counter
    for string in strings:
        if len(string) > maxlength:
            maxlength = len(string)

    # Extract radix, starting from the end of the largest string
    for i in range(maxlength):
        for string in sortedStrings:
            if maxlength - len(string) <= i:
                sorter[sortingDict[string[maxlength - i - 1]] + 1].append(string)
            else:
                sorter[0].append(string)

        sortedStrings = []

        # Reconstruct array with current radix sorted
        for i in range(len(sorter)):
            sortedStrings.extend(sorter[i])
            sorter[i] = []

    return sortedStrings

names = []

with open("files/names.txt","r") as rf:
    for line in rf:
        line = re.sub('["\n]', "", line)
        line = re.split(",",line)

        names.extend(line)

names = radixSortStrings(names, letters)

total = 0

for i in range(len(names)):
    result = 0

    for letter in names[i]:
        result += letters[letter] + 1
    
    total += result * (i+1)

print(total)