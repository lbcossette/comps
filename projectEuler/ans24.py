import math

"""
Problem :
A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:

012   021   102   120   201   210

What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?

This does not require an actual program to solve (other than to calculate factorials) : we number permutations starting from 0, group all digits in an array, in order,
and perform the following for the k-th:
- k_0 = k-1
- Given that there are (n-1)! permutations of numbers past the first digit, calculate floor(k_0/(n-1)!). This will give the index of the correct first digit in the array.
- We reduce : k_1 = k_0 % (n-1)!
- We take that digit out of the array and note it somewhere.
- We go for the second digit. This time there are (n-2)! digit permutations past the second, so we calculate floor(k_1/(n-2)!) to get the correct index for the second digit.
- We reduce : k_2 = k_1 % (n-2)!
- We take the correct digit out of the array and note it somewhere.
- Etc.

This works only if the digits are stated in ascending order in the initial array.
"""

n = 11*math.factorial(15)
permNumber = n-1

digits = [0,1,2,3,4,5,6,7,8,9,"A","B","C","D","E","F"]
nDigits = len(digits)

sequence = []

for i in range(1, nDigits + 1):
    sequence.append(digits.pop(int(permNumber/math.factorial(nDigits - i))))
    permNumber = permNumber % math.factorial(nDigits - i)

print(sequence)