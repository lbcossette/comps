from pythagoras import genTriples
from primes import listPrimes
import math

"""
Because max is reserved, we will baptize max as marx,
and will find triples up to the marximum
"""

marx = 1500001

# Generate all primitive triples
triples = genTriples(marx)

# Count occurence of each perimeter
occurences = dict()

for triple in triples:
    primitive = triple[0] + triple[1] + triple[2]

    k = 1

    perim = k*primitive

    while perim < marx:
        if not perim in occurences:
            occurences[perim] = 1
        else:
            occurences[perim] += 1

        k += 1
        perim = k*primitive

nUniques = 0

for occurence in occurences:
    if occurences[occurence] == 1:
        nUniques += 1

print(nUniques)