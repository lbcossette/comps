from primes import listPrimes, primeFactorsRange, fromPrimesToAllDivisors, coprime
import math

"""
Question :
A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.
By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers.
However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot
be expressed as the sum of two abundant numbers is less than this limit.

Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
"""
primeList = listPrimes(28123)
divisorDict = fromPrimesToAllDivisors(primeFactorsRange(1, 28123, primeList))

"""
Finding all primitive abundant numbers.

Because every dividor c of n also divides kn and because for every c,
kc divides kn, we have that sumOfDivisors(n) > k*sumOfDivisors(n),
therefore all multiples of abundant numbers are abundant numbers.
"""
primitiveAbundants = []
otherAbundants = []

for i in divisorDict:
    divisorDict[i] = sum(divisorDict[i])

    if divisorDict[i] > i:
        candidate = i
        primitive = True

        for primAbundant in primitiveAbundants:
            if i % primAbundant == 0:
                primitive = False
                break

        if primitive:
            primitiveAbundants.append(i)

# Remove all multiples of primitive abundant numbers
# from list of numbers not expressible by a sum of
# abundant numbers
for primAbundant in primitiveAbundants:
    for i in range(2,int(28123/primAbundant + 1)):
        if i*primAbundant in divisorDict:
            divisorDict.pop(i*primAbundant)

removed = []

# Remove all combinations of multiples of primitive abundant numbers
for i in range(25, 28124):
    if i in divisorDict:
        print(i)
        if i in removed:
            raise Exception("Did not pop correctly!")
        found = False
        for j in range(len(primitiveAbundants)):
            if primitiveAbundants[j] > i:
                break
            for k in range(1, int((28123-18)/primitiveAbundants[j] + 1)):
                residue = i - k*primitiveAbundants[j]

                for l in range(j+1, len(primitiveAbundants)):
                    if primitiveAbundants[l] > residue:
                        break
                    if residue % primitiveAbundants[k] == 0:
                        found = True
                        for m in range(1, int(28123/i + 1)):
                            if m*i in divisorDict:
                                divisorDict.pop(m*i)
                                removed.append(m*i)
                        break

                if found:
                    break

            if found:
                break

print(divisorDict)