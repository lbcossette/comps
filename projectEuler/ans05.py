import math
from primes import listPrimes
from functools import reduce

"""
Question :
What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
"""

def enumerate(high):
    """
    Enumerate all primes with multiplicity of their highest power below high.

    The idea is that if a number is to be evenly divided by all numbers
    between 1 and n, then that number must be divided by their prime number
    decomposition. To obtain the smallest prime number decomposition that evenly
    divided by all primes number decompositions between 1 and n, one must simply
    take the highest power of each prime found in these prime number decompositions.

    Indeed, suppose a divisor of our target number has, for example 2^3 as part of
    its prime decomposition, then either 3 is the largest power of 2 of all numbers
    between 1 and n, or it is not. If it is, then the prime number decomposition of
    the target will have 3 as a power of 2. If not, then it will be higher, and can
    therefore be divided evenly by 2^3. Same goes for all other powers of primes, so
    if a number has, for example 2^3 * 5^2 in its prime number decomposition, then
    the target will be divided evenly by that number.
    """
    
    primeList = listPrimes(high)

    factors = []

    for prime in primeList:

        if prime * prime < high:
            power = prime

            primePowers = [prime]

            while power < high:
                power  = power * prime
                primePowers.append(prime)

            primePowers.pop()

            factors.extend(primePowers)

        else:
            factors.append(prime)
            
    return factors

ans = enumerate(20)

print(ans)

print(reduce(lambda x,y : x * y, ans))