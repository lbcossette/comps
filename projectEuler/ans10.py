from primes import listPrimes
from functools import reduce

"""
Question :
Find the sum of all the primes below two million.
"""

print(reduce(lambda x,y : x + y, listPrimes(2000000)))