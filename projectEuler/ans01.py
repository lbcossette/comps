import math
from primes import LCM

"""
Question :
Find the sum of all the multiples of 3 or 5 below 1000.
"""

mult1 = 3
mult2 = 5
lim = 999

def sumMultiples(a,b,n):
    """
    Find the sum of multiples of a or b from 1 to n:

    Considering that:
    a + 2a + 3a + ... = (1 + 2 + 3 + ...)a,
    then the sum is worth :
    floor(n/a)*(floor(n/a)+1)*a/2

    Same goes for b. Because multiples of the LCM of a and b
    are summed twice, their sum is substracted from the result.

    The algorithm if of O(1) complexity.
    """
    multsOfA = math.floor(n/a)
    multsOfB = math.floor(n/b)

    lcmAB = LCM(a,b)

    multsOfAnB = math.floor(n/lcmAB)

    multsOfA = multsOfA * (multsOfA + 1) * a/2
    multsOfB = multsOfB * (multsOfB + 1) * b/2
    multsOfAnB = multsOfAnB * (multsOfAnB + 1) * lcmAB/2

    return int(multsOfA + multsOfB - multsOfAnB)


print(sumMultiples(mult1,mult2,lim))