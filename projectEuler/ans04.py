import math

"""
Question :
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.

Heuristic :
The largest such palindrome has six digits and is very close from 999999
"""

# Helper functions

def nextPalindrome(digits):
    """
    Because the number is a palindrome, we need to substract from the middle
    towards the ends of the number. If not, we will substract higher powers
    of 10 and therefore will not get a descending order sequence.
    e.g. 99999 > 99899 > 98989 > 89998
    or e.g. 99099 = 99999 - 9*10^2, 98989 = 99999 - 10^3 - 10^1, 1010 > 900
    """

    j = len(digits) >> 1

    i = 0

    l = len(digits)-1

    if len(digits) % 2 == 1:
        i = j
    else:
        i = j-1

    while j < l and digits[j] == 0:
        digits[j] = 9
        digits[i] = 9
        j += 1
        i -= 1

    if digits[j] == 0:
        return digits

    digits[j] -= 1

    if i != j:
        digits[i] -= 1

    return digits

def toDecimal(digits):
    """
    Simple decimal conversion :
    (a3)(a2)(a1)(a0) = sum of i from 0 to 3 of (ai * 10^i)
    """
    num = 0

    for i in range(len(digits)):
        num += digits[i] * math.pow(10,i)

    return int(num)

def tryDivisions(num,low,high):
    for i in range(low,high+1):
        if num % i == 0 and int(num / i) < high:
            num = int(num / i)

            return (num, i)
    
    return False

# The actual problem

palindrome = [9,9,9,9,9,9]

ndigits = math.ceil(math.log10(toDecimal(palindrome) + 1))

divisorDigits = ndigits >> 1

low = int(math.pow(10,divisorDigits-1)) # The first digit is 1. The power of 10 is the number of zeros
high = int(math.pow(10,divisorDigits) - 1)

print("{:n} digits, divisors from {:n} to {:n}".format(ndigits,low,high))

divisors = False

while(not divisors):
    current = toDecimal(palindrome)

    divisors = tryDivisions(current,low,high)

    palindrome = nextPalindrome(palindrome)

print(divisors[0]*divisors[1])
print(divisors)