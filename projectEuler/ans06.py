import math

"""
Question :
Find the difference between the sum of the squares of the first one hundred natural numbers and the square of their sum.
"""

def sumsquares(low,high):
    """
    The formula is obtained the following way :
    sum(i=1,n,i^3) - sum(i=1,n,(i-1)^3)
    = sum(i=1,n,i^3) - sum(i=1,n, i^3 - 3i^2 + 3i - 1)
    = sum(i=1,n, 3i^2 - 3i + 1)
    = sum(i=1,n,i^3) - sum(j=0,n-1,j^3)
    = sum(i=n,n,i^3) = n^3

    The rest sum separation and result factorization
    with sum(i=1,n, i) = n(n+1)/2, and is considered trivial
    """
    
    if low == high or int(low) != low or int(high) != high:
        return 0
    elif low > high:
        return sumsquares(high,low)
    elif low < 0 and high < 0:
        return sumsquares(-1*high,-1*low)
    elif low < 0 and high >= 0:
        return sumsquares(0,-1*low) + sumsquares(0,high)
    elif low > 0:
        return sumsquares(0,high) - sumsquares(0,low-1)
    else:
        return int(high*(high+1)*(2*high+1)/6)

print((100*101/2)**2 - sumsquares(0,100))