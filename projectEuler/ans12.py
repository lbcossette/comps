from primes import primeFactors
import math
from copy import deepcopy

"""
Question :
What is the value of the first triangle number to have over five hundred divisors?
"""

def productFactors(facts1, facts2):
    newfactors = []

    factors1 = deepcopy(facts1)
    factors2 = deepcopy(facts2)

    while len(factors1) > 0 and len(factors2) > 0:
        if factors1[-1][0] > factors2[-1][0]:
            newfactors.append(factors1.pop())
        elif factors1[-1][0] < factors2[-1][0]:
            newfactors.append(factors2.pop())
        else:
            newfactors.append(factors1.pop())
            newfactors[-1][1] += factors2.pop()[1]

    while len(factors1) > 0:
        newfactors.append(factors1.pop())
    while len(factors2) > 0:
        newfactors.append(factors2.pop())

    newfactors.reverse()

    return newfactors


low = 2
high = 20000

factors = primeFactors(low,high)

for i in range(low,high):
    trigIFactors = productFactors(factors[i],factors[i+1])
    trigIFactors[0][1] -= 1
    
    nDivisors = 1

    for factor in trigIFactors:
        nDivisors *= factor[1] + 1

    if nDivisors >= 500:
        print("{:n}-th triangular number : {:n}, {:n} divisors.".format(i, int(i*(i+1)/2), nDivisors))

"""

"""