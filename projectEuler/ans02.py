import math

"""
Question :
By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.
"""

"""
Sum of all even fibonacci numbers between 1 and n.

(Starting at 1,2 instead of 0,1)

Because F(n) = F(n-1) + F(n-2), F(n) will be even only if
F(n-1) and F(n-2) are either both even or both odd. Assuming
0 is even, we get even,odd,odd,even,odd,odd,even...

Only the fibonacci numbers at positions of multiples of 3 will be even

Using the formula floor((phi^n)/sqrt(5) + 1/2), we get the n-th
fibonacci number.
"""

sqrt5 = math.sqrt(5)

phi = (1 + sqrt5)/2

def fibo(n):
    return math.floor(math.pow(phi,n)/sqrt5 + 0.5)

def evenFiboSum(lim):
    n = 3
    summa = 0

    while(True):
        term = fibo(n)
        if term > lim:
            break

        print(term)
        summa += term
        n+=3

    return summa

print(evenFiboSum(4000000))

n0 = 0
n1 = 1
tot = 0

while n1 < 4000000:
    if n1 % 2 == 0:
        tot += n1
    
    tmp = n1 + n0
    n0 = n1
    n1 = tmp

print(tot)