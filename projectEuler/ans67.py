import re
from ans18 import nextMaxSums

triangle = []
with open("files/triangle.txt", "r") as rf:
    for line in rf:
        line = list(map(lambda x : int(x), re.split(r"\s", line.rstrip())))
        triangle.append(line)

currMaxSums = triangle[0]

for i in range(1,len(triangle)):
    currMaxSums = nextMaxSums(currMaxSums, triangle[i])
    #print(currMaxSums)

print(currMaxSums)
print(max(currMaxSums))