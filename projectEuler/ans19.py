
def julDayNum(year,month,day):
    """
    Formula for calculating the Julian Day Number from the Gregorian calendar date.

    Source :
    https://en.wikipedia.org/wiki/Julian_day#Converting_Gregorian_calendar_date_to_Julian_Day_Number
    (L. E. Doggett, Ch. 12, "Calendars", p. 604, in Seidelmann 1992)
    """
    return int((1461*(year + 4800 + int((month-14)/12)))/4) + int((367*(month-2 - 12*int((month-14)/12)))/12) - int((3*int((year+4900 + int((month-14)/12))/100))/4) + day - 32075

# Was a Monday, and Mondays are = 0 (mod 7)
firstDay = julDayNum(1900,1,1)

firstDaySundays = 0

for i in range(1901,2001):
    for j in range(1,13):
        # If Mondays are = 0 (mod7), then Sundays are = 6 (mod 7)
        if (julDayNum(i,j,1) - firstDay)%7 == 6:
            firstDaySundays += 1

print(firstDaySundays)