from primes import listPrimes, primeFactorsRange, fromPrimesToAllDivisors

"""
Question :
Evaluate the sum of all the amicable numbers under 10000
"""

# List primes up to sqrt(10000)
myPrimes = listPrimes(10000)

#print(myPrimes)

divisorDict = primeFactorsRange(4,9999,myPrimes)

divisorDict = fromPrimesToAllDivisors(divisorDict)

for i in divisorDict:
    divisorDict[i] = sum(divisorDict[i])

amicables = []

for i in divisorDict:
    # Iterate dict to identify amicable pairs
    if i in amicables or i in myPrimes:
        continue
    
    if (divisorDict[i] in divisorDict) and i != divisorDict[i] and divisorDict[divisorDict[i]] == i:
        print("{:n} : {:n}".format(i, divisorDict[i]))
        amicables.append(i)
        amicables.append(divisorDict[i])

print(sum(amicables))