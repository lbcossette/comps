"""
Could be converted into binary-coded 32-bit
unsigned integers per array slot for extra efficiency,
with a bool for positive/negative, or something.

e.g. : 2^64 - 1 = [2^32 - 1, 2^32 - 1], sgn = True
e.g. : -2^64 + 1 = [2^32 - 1, 2^32 - 1], sgn = False
"""

class largeInt:
    def __init__(self,numString):
        """
        Represents an unsigned integer as an array of digits.

        Could be converted into binary-coded 32-bit
        unsigned integers per array slot for extra efficiency,
        with a bool for positive/negative, or something.

        e.g. : 2^64 - 1 = [2^32 - 1, 2^32 - 1], sgn = True
        e.g. : -2^64 + 1 = [2^32 - 1, 2^32 - 1], sgn = False
        """

        self.representation = list(map(lambda x : int(x), numString))
        self.representation.reverse()
        self.trimZeroes()

    def propagateRemainders(self,i):
        self.representation[i+1] += int(self.representation[i]/10)
        self.representation[i] = self.representation[i]%10

    def checkLastDigit(self):
        while self.representation[-1] > 9:
            self.representation.append(int(self.representation[-1]/10))
            self.representation[-2] = self.representation[-2]%10

    def trimZeroes(self):
        while self.representation[-1] == 0 and len(self.representation) > 1:
            self.representation.pop()

    def add(self,num):
        def digitWiseAdd():
            for i in range(len(num.representation) -1):
                res = self.representation[i] + num.representation[i]

                self.representation[i] = res%10
                self.representation[i+1] += int(res/10)

            self.representation[len(num.representation) - 1] += num.representation[-1]

        if len(self.representation) < len(num.representation):
            self.representation.extend(list((0,)*(len(num.representation) - len(self.representation))))

        digitWiseAdd()
        for i in range(len(num.representation) - 1, len(self.representation) - 1):
            self.propagateRemainders(i)
        self.checkLastDigit()

    def multiply(self,num):
        # Reserving minimal result space
        result = list((0,)*(len(self.representation) + len(num.representation) - 1))

        # Creating the linear convolution of the two digit lists
        for i in range(len(num.representation)):
            for j in range(len(self.representation)):
                result[i+j] += num.representation[i]*self.representation[j]

        self.representation = result

        # Propagating the remainders
        for i in range(len(result) - 1):
            self.propagateRemainders(i)

        # Solving last remainders and handling multiplication by zero
        self.checkLastDigit()
        self.trimZeroes()

    def toString(self):
        return "".join(map(lambda x: str(x), self.representation))[::-1]

def testLargeInt():
    # Test for addition remainder handling
    test1 = largeInt("987654321")
    test2 = largeInt("123456789")
    test1.add(test2)
    if test1.toString() != "1111111110":
        raise Exception("Error! Doing 987654321 + 123456789 yields {:s} and not 1111111110.".format(test1.toString()))

    # Test for addition commutativity
    test3 = largeInt("987654321")
    test2.add(test3)
    if test1.toString() != test2.toString():
        raise Exception("Error! Addition is not commutative!")

    # Test for multiplication remainder handling
    test1 = largeInt("987654321")
    test2 = largeInt("123456789")
    test1.multiply(test2)
    if test1.toString() != "121932631112635269":
        raise Exception("Error! Doing 987654321 * 123456789 yields {:s} and not 121932631112635269.".format(test1.toString()))

    # Test for multiplication commutativity
    test3 = largeInt("987654321")
    test2.multiply(test3)
    if test1.toString() != test2.toString():
        raise Exception("Error! Multiplication is not commutative!")

    # Test for addition of ints of differing size
    test1 = largeInt("987654321")
    test2 = largeInt("1")
    test1.add(test2)
    if test1.toString() != "987654322":
        raise Exception("Error! Doing 987654321 + 1 yields {:s} and not 987654322.".format(test1.toString()))

    # Same, but for commutativity
    test3 = largeInt("987654321")
    test2.add(test3)
    if test1.toString() != test2.toString():
        raise Exception("Error! Addition is not commutative when two integers are not of the same size!")

    # Test for multiplication of ints of differing size
    test1 = largeInt("987654321")
    test2 = largeInt("1")
    test1.multiply(test2)
    if test1.toString() != "987654321":
        raise Exception("Error! Doing 987654321 * 1 yields {:s} and not 987654321.".format(test1.toString()))

    # Same, but for commutativity
    test3 = largeInt("987654321")
    test2.multiply(test3)
    if test1.toString() != test2.toString():
        raise Exception("Error! Multiplication is not commutative when two integers are not of the same size!")

    # Multiplication by 0
    test1 = largeInt("987654321")
    test2 = largeInt("0")
    test1.multiply(test2)
    if test1.toString() != "0":
        raise Exception("Error! Doing 987654321 * 0 yields {:s} and not 0.".format(test1.toString()))

    # Addition of 0
    test1 = largeInt("987654321")
    test2 = largeInt("0")
    test1.add(test2)
    if test1.toString() != "987654321":
        raise Exception("Error! Doing 987654321 + 0 yields {:s} and not 987654321.".format(test1.toString()))

testLargeInt()