"""
Question :
The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?
"""

"""
The process goes by elimination to reduce the ammount of computation necessary :
If a given candidate leads to another in a Collatz chain, the length of that number's chain
is certainly smaller than the length of the starting number. Indeed, the behavior of
that chain is deterministic when the starting point is known. 
"""

lim = 1000000

candidates = list(range(lim))

maxNum = 0
maxLength = 0

for i in range(1,len(candidates) + 1):
    # Consider only nonexcluded candidates
    if candidates[-i] != 0:
        chainLength = 0

        # Run chain and record chain length
        while candidates[-i] > 1:
            if candidates[-i] % 2 == 0:
                candidates[-i] = int(candidates[-i] / 2)
            else:
                candidates[-i] = 3*candidates[-i] + 1
            
            chainLength += 1

            # Exclude any candidate in the current chain
            if candidates[-i] < lim:
                candidates[candidates[-i]] = 0

        if chainLength > maxLength:
            maxLength = chainLength
            maxNum = lim - i

    elif (lim-i)*2 > lim:
        print(lim - i)

print(maxNum)
print(maxLength)