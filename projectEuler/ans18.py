from copy import copy

"""
By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

3
7 4
2 4 6
8 5 9 3

That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom of the triangle below:

75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
"""

"""
Solution :
Calculate that maximum path sum to each element of level i,
then i+1.
Knowing that the maximum path to each element of level i
has already been found, there are only at most two checks
by element of level i+1. This check is to find which element
above it has the largest maximum path sum, and choose to append
itself to that path instead of the other in order to maximize its
own path sum.
This creates a binary tree for which only one branch per level
has two children, and therefore means there are only n values
to check at the end instead of somewhere near to 2^(n-1).
"""

def nextMaxSums(currMaxSums, levelValues):
    newMaxSums = copy(levelValues)

    if not currMaxSums:
        if len(levelValues) == 1:
            return levelValues
        else:
            raise Exception("This is not the tip of the triangle!")

    # Only one path possible for edges of triangle
    newMaxSums[0] += currMaxSums[0]
    newMaxSums[-1] += currMaxSums[-1]

    # Choose which current maximal sum is the highest
    # that reaches the current number
    for i in range(1,len(levelValues)-1):
        if currMaxSums[i-1] > currMaxSums[i]:
            newMaxSums[i] += currMaxSums[i-1]
        else:
            newMaxSums[i] += currMaxSums[i]

    return newMaxSums

# The code has been commented and the function above has been recycled for ans67.py
"""
triangle = [[75],
            [95, 64],
            [17, 47, 82],
            [18, 35, 87, 10],
            [20, 4, 82, 47, 65],
            [19, 1, 23, 75, 3, 34],
            [88, 2, 77, 73, 7, 63, 67],
            [99, 65, 4, 28, 6, 16, 70, 92],
            [41, 41, 26, 56, 83, 40, 80, 70, 33],
            [41, 48, 72, 33, 47, 32, 37, 16, 94, 29],
            [53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14],
            [70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57],
            [91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48],
            [63, 66, 4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31],
            [4, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 4, 23]]

currMaxSums = [75]

for i in range(1,len(triangle)):
    currMaxSums = nextMaxSums(currMaxSums, triangle[i])
    print(currMaxSums)

print(max(currMaxSums))
"""