def genTriples (marx,a=3,b=4,c=5):
    """
    Generate primitive pythagorean triples such that
    their sum is lower than the marximum.
    This is a recursive function for reasons that
    will become obvious.

    Using matrix multiplication to generate triples :
    Hall (1970) and Roberts (1977)
    http://mathworld.wolfram.com/PythagoreanTriple.html

    U = [[ 1, 2, 2]
         [-2,-1,-2]
         [ 2, 2, 3]]
    A = [[ 1, 2, 2]
         [ 2, 1, 2]
         [ 2, 2, 3]]
    D = [[-1,-2,-2]
         [ 2, 1, 2]
         [ 2, 2, 3]]

    Primitive triples are generated from :
    [3,4,5]M, where M is a finite product of U, A and D
    """

    triples = []

    if a+b+c < marx:
        triples.append([a,b,c])
        triples.extend(genTriples(marx,a - 2*b + 2*c, 2*a - b + 2*c, 2*a - 2*b + 3*c))
        triples.extend(genTriples(marx,a + 2*b + 2*c, 2*a + b + 2*c, 2*a + 2*b + 3*c))
        triples.extend(genTriples(marx,-a + 2*b + 2*c, -2*a + b + 2*c, -2*a + 2*b + 3*c))

    return triples