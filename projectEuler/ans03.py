import math
from primes import listPrimes

"""
Question :
Find largest prime factor of 600851475143
"""

"""
This most likely isn't optimal, but here it goes :
- List all primes between 2 and sqrt(n)
- Check for all prime factors
"""

lim = 600851475143

primes = listPrimes(math.sqrt(lim))

largestPrimeFactor = 0

for i in range(len(primes)):
    largestPrimeFactor = primes[i]

    while lim % largestPrimeFactor == 0:
        print(largestPrimeFactor)
        lim = int(lim / largestPrimeFactor)

    if lim == 1:
        break

print(largestPrimeFactor)