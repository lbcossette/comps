import math
from primes import listPrimes

"""
Question :
What is the 10 001st prime number?
"""

print(listPrimes(math.inf, 10001)[10000])