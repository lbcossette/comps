import math

"""
Using Euler's formula :
Base case : p(0) = 1
p(n) = sum(k != 0, n - k*(3k-1) >= 0, (-1)^k * p(n - k*(3k-1)))

Another generalized solution for direct p(n) calculation is found in :
Johansson F (2012) Efficient implementation of the Hardy–Ramanujan–Rademacher formula. LMS J Comput Math. 15:341-359
This algorithm is O(n^(0.5+C)) with C a constant.
"""

partitions = [1]

n = 1

while partitions[-1] != 0:
    k = 1

    pent1 = int(k*(3*k-1)/2)
    pent2 = int(k*(3*k+1)/2)

    newP = 0

    while pent1 <= len(partitions):
        sign = 1
        
        if k & 1 == 0:
            sign = -1


        newP += sign*partitions[-pent1]

        if pent2 <= len(partitions):
            newP += sign*partitions[-pent2]

        k += 1

        pent1 = int(k*(3*k-1)/2)
        pent2 = int(k*(3*k+1)/2)

    # To get the actual number, remove %1000000
    partitions.append(newP%1000000)

    n += 1

print(n-1)