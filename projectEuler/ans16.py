from functools import reduce

"""
Question :
What is the sum of the digits of the number 2^1000?
"""

print(reduce(lambda x,y : x+y, list(map(lambda x : int(x), str(2**1000)))))