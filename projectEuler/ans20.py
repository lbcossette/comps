from largeNumbers import largeInt
from functools import reduce

wantedFactorial = 100

result = largeInt(str(wantedFactorial))

for i in range(1, wantedFactorial - 1):
    result.multiply(largeInt(str(wantedFactorial - i)))
    
print(result.toString())
print(reduce(lambda x,y : x+y, result.representation))