import math

"""
Problem :
It is easily proved that no equilateral triangle exists with integral length sides and integral area. However, the almost equilateral triangle 5-5-6 has an area of 12 square units.

We shall define an almost equilateral triangle to be a triangle for which two sides are equal and the third differs by no more than one unit.

Find the sum of the perimeters of all almost equilateral triangles with integral side lengths and area and whose perimeters do not exceed one billion (1,000,000,000).
"""



maxPerimeter = 10000

maxN = int((maxPerimeter - 2) / 3 + 1)

for i in range(1,maxN):
    test1 = math.sqrt(3*i*i + 2*i)
    test2 = math.sqrt(3*i*i + 4*i + 1)

    if float(int(test1)) == test1:
        b = (math.sqrt(3*test1*test1 + 1) + 2)/3

        print("{:n} : {:n} -> {:f}".format(i, int(test1), b))

    if float(int(test2)) == test2:
        b = (math.sqrt(3*test1*test1 + 1) + 2)/3

        print("{:n} : {:n} -> {:f}".format(i, int(test2), b))