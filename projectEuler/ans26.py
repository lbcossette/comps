from primes import primeFactorsRange

"""
Problem :
A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:

1/2	= 	0.5
1/3	= 	0.(3)
1/4	= 	0.25
1/5	= 	0.2
1/6	= 	0.1(6)
1/7	= 	0.(142857)
1/8	= 	0.125
1/9	= 	0.(1)
1/10	= 	0.1
Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.

Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.

The answer would be the largest prime below 1000, as (k^n) % p = (k^(n+c)) % p with p prime, and k > 1 iff c is
a multiple of p-1 (with k != 0).
You can think of the set {0,...,p-1} as a ring with + and * (%p) and demonstrate that as long as k is not a multiple of p,
kr % p == kr' % p means that either r = 0 or r == r'. It follows that consecutively multiplying some nonzero remainder
by the same number will generate a sequence cycling through all values of {1,...,p-1}, and them come back to its original value,
giving a cycle of length p-1. Therefore, the largest prime number before 1000 (983) should give the largest cycle, as other
numbers are not guaranteed to cycle through ALL values of {1,...,n-1}, meaning that n-1 is an upper bound for cycle length.

This is also a very primitive random number generator, come to think of it.

Here is code to demonstrate it.
"""


factors = primeFactorsRange(3,999)

for i in range(3,1000):
    bad = True
    for factor in factors[i]:
        if factor[0] != 5 and factor[0] != 2:
            bad = False

    if bad:
        factors.pop(i)

sequences = []

for i in factors:
    #print(i)
    sequence = []
    carry = 1
    hasPeriod = False

    while True:
        # Generation of decimal representation of the fraction in base 10
        # Actually, we do not care about the numbers is the sequence,
        # just its length, so we only store the remainders we need to continue.
        carry = (carry*10) % i

        # Check if there is some period in the sequence, or if the sequence ended.
        if carry == 0:
            break
        elif carry in sequence:
            hasPeriod = True
            break
        else:
            sequence.append(carry)

    if hasPeriod:
        sequences.append((i, len(sequence) - sequence.index(carry)))
    else:
        sequences.append((sequence,0))

maxSequence = (0,0)

for sequence in sequences:
    if sequence[1] > maxSequence[1]:
        maxSequence = sequence

print(maxSequence)