import math

def checkPrime(n,primes):
    for j in range(len(primes)):
        if primes[j]*primes[j] > n:
            return True

        if n % primes[j] == 0:
            return False

def listPrimes(lim, maxlength=math.inf):
    """
    List all primes between 2 and lim, inclusively,
    up to maxlength primes total.
    """

    if maxlength <= 0:
        return []
    if maxlength == 1 or lim == 2:
        return [2]
    if maxlength == 2 or lim == 3:
        return [2,3]
    if maxlength == math.inf and lim == maxlength:
        raise Exception("Will not list all primes up to infinity. Please reconsider your options.")

    primes = [2,3]

    n = 5

    while n <= lim and len(primes) <= maxlength:
        if checkPrime(n,primes):
            primes.append(n)
        
        n = n + 2

        if checkPrime(n,primes):
            primes.append(n)

        n = n + 4
    
    return primes

def GCD(a,b):
    """
    Using Euclid's algorithm to identify the GCD of a and b
    """
    while a != 0 and b != 0:
        if a > b:
            a = a % b
            continue
        elif a < b:
            b = b % a

    if a == 0:
        return b
    else:
        return a

def LCM(a,b):
    """
    Finding the LCM using a, b and GCD(a,b)
    """

    return int(a*b / GCD(a,b))

def primeFactorsRange(low,high, primeList=[]):
    """
    Identify all prime factors with multiplicity
    for all numbers from low to high, inclusively.
    """
    if not primeList:
        primeList = listPrimes(high)

    factors = dict()

    for i in range(low,high+1):
        n = i

        iFactors = []

        if not i in primeList:
            for candidate in primeList:
                if candidate > n:
                    break

                multiplicity = 0

                while n % candidate == 0:
                    multiplicity += 1

                    n = int(n/candidate)
        
                if multiplicity > 0:
                    iFactors.append([candidate,multiplicity])

                if n == 1:
                    break
        else:
            iFactors = [[i,1]]

        factors[i] = iFactors

    return factors

def primeFactors(N, primeList=[]):
    if not primeList:
        primeList = listPrimes(N/2)

    factors = []

    for candidate in primeList:
        if candidate > N:
            break
        
        multiplicity = 0

        while N % candidate == 0:
            multiplicity += 1

            N = int(N/candidate)

        if multiplicity > 0:
            factors.append([candidate,multiplicity])

        if N == 1:
            break

    return factors

def fromPrimesToAllDivisors(divisorDict):
    for i in divisorDict:
        #print(i)
        # Generator of all factors : start at 1
        trueFactors = [1]
        #print(divisorDict[i])

        # For every new divisor we have the choice to multiply
        # all other previously identified divisors by either
        # of the divisor's powers, and doing so generating new
        # divisors. The choice of "no power of that divisor" is
        # covered by iterations over previous divisors.
        for j in range(len(divisorDict[i])):
            primeDivisor = divisorDict[i][j][0]

            for k in range(len(trueFactors)):
                for l in range(1,divisorDict[i][j][1]+1):
                    trueFactors.append(trueFactors[k]*(primeDivisor**l))

        # The last element of that list is the complete number,
        # which we do not want
        trueFactors.pop()
        divisorDict[i] = trueFactors

        #if i == 220 or i == 284:
            #print(divisorDict[i])
        
    return divisorDict

def coprime(n1, n2, primeList=[]):
    # Check if numbers are equal, multiples of each other or both even
    if n1 == n2 or n1 % n2 == 0 or (n1 & 1 == 0 and n2 & 1 == 0):
        return False

    minN = min(n1,n2)

    # The maximal divisor of maxN is maxN/2. If minN
    # is greater than maxN/2, then it is not divisible
    # by maxN/2 unless minN = maxN. If minN = maxN / 2,
    # then minN is a divisor of maxN. Remains the case
    # where minN < maxN/2. minN has minN/2 as its maximal
    # proper divisor
    if not primeList:
        primeList = listPrimes(int(minN/2 + 1))

    for prime in primeList:
        if prime == 2:
            continue
        if n1 % prime == 0 and n2 % prime == 0:
            return False

        if prime*2 > minN:
            break

    return True