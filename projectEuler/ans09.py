import math
from pythagoras import genTriples

"""
Question :
There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.

By careful manipulation of this system :
a^2 + b^2 = c^2
a + b + c = r
a < b < c < a + b (the last inequality is there because the object is a triangle)

We obtain :
sqrt(13)*r/6 > c > (sqrt(3) - 1)*r/2
(r-a)/2 > b > (r-a)*r/(2*(r+a))
r/3 > a > 0
"""

target = 1000

lowA = 0#int(target/2 * (2 - math.sqrt(2)) + 1)

highA = int(target / 3 + 1)

for a in range(lowA, highA):
    lowB = int((target-a)*target/(2*(target+a)) + 1)

    highB = int((target-a)/2 + 1)

    for b in range(lowB, highB):
        c = target - a - b
        if a*a + b*b == c*c:
            print("{:n}^2 + {:n}^2 = {:n}^2 and {:n} + {:n} + {:n} = {:n}.".format(a,b,c,a,b,c,target))
            print("Product : {:n}".format(a*b*c))

"""
Problem solved here using explicit triple generation using constrains for c
"""

triples = genTriples(int((math.sqrt(3) - 1)*target/2 + 1), int(math.sqrt(13)*target/6 + 1))

for triple in triples:
    a = triple[0]
    b = triple[1]
    c = triple[2]

    if a+b+c == target:
        print(a*b*c)

"""
Another potential solution using general pythagorean triple generation formula :
a + b + c = r
k(m^2 - n^2) + 2kmn + k(m^2 + n^2) = r
2km^2 + 2kmn = r
2km(m+n) = r

m(m+n) = r/(2k)

From k = 1 while k <= r/24 (the smallest pythagorean triple has a+b+c = 12):
    # If k is square, then m' = km and n' = km are solutions
    Skip if r/2k is not an integer or k is a square number
    From m = 2 while m < sqrt(r/(2k)):
        From n = 1 while n < m:
            if 2km(m+n) = r:
                print(a*b*c)
                end program
"""